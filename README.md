# SLIME

 - 小規模〜中規模サイト制作に適した柔軟なテンプレート


| 前提パッケージ   |
| :-----------: |
| Node.js       |
| Ruby          |
| Sass(Compass) |
| Gulp          |

| Gulp Tasks  | 説明|
| :-----------: | :-----------: |
| imagemin | 画像圧縮 |
| browserify | JSコンパイル |
| compass | Scssコンパイル|
| kss | スタイルガイド作成|
| jsdoc | jsdoc作成 |
| uglify | JS圧縮|
| webserver | ローカルサーバーの起動 |
| watch | ディレクトリ監視・タスク連結|
| build | 本番用出力|

 - プロジェクト情報もREADME.mdを作って書いて欲しい
 
 - リクエスト数削減のため、JS/CSSはそれぞれ１本化されます。
 
 - 基本思想としてbrowserifyが使用・推奨されていますが、選択しないことも可能です。
 - １本化されたJS/CSSの書き出し等はデフォルトで下記ディレクトリ・名称に指定されています。
 - タスクを変更したい場合は/gulpfile.jsを編集してください。
 
 - /common/js/script.js
 - /common/css/style.css

## HTML Template
 - ejsを採用
   - 静的インクルードが可能
   - TDK・OGP等サイト設定を外部ファイル化
 
 - サイト設定はsiteconfig.jsonに記述

## Javascript
 - Browserify
   - グローバルにjQueryを宣言するため、jQuery Pluginの使用が可能。(非選択・Zepto等の使用も可)
 
 - Utility,Moduleの概念
   - Utility : 再利用される汎用的なスクリプト。Utilityのjsdocで定義される。
   - Module  : 再利用されない専門のスクリプト。Moduleのjsdocで定義される。

### Utility-script

| Utilty Script | 説明| 依存ライブラリ|
| :-----------: | :-----------: | -----------: |
| isMobile.js  | モバイル判定 | |
| existy.js | 存在判定 | lodash|
   
### JSDoc

## StyleSheets

 - 使用したいUtility, Moduleのスタイル(SCSS)を共通SCSSから呼び出し結合します。

 - SCSS(SMACSS)
   - Base, Layout, Module, State, Themeそれぞれで分割された.Scssファイル
 
 - Utility,Moduleの概念
   - Utility : 再利用される汎用的なスタイル。Utilityのスタイルガイドで定義される。
   - Module  : 再利用されない専門のスタイル。Moduleのスタイルガイドで定義される。

### Utility-style

| Utilty Style | 説明|
| :-----------: | :-----------: |
| _margin.scss  | マージン調整用 |
| _padding.scss | パディング調整用 |
| _grid.scss    | Bootstrapグリッドシステム |

- Utility-KSSの設置

### Style Guide (KSS)

## Directory

 - srcディレクトリの構成はdistの構成と同じになる。ただし開発用モジュール等は出力されない。
 
 - dist : 本番出力用ディレクトリ
   
 - lib : ライブラリ格納ディレクトリ
   
 - src : 開発用ディレクトリ
   
## feature

 - webフォントの置き場 、fontello等更新しやすく・管理しやすく
 
 - インストールの簡略化
   - node.jsインストール・gulpグローバルインストール・sassインストール
 
 - Google apps scriptsでsiteconfig.jsonを生成する
 
 - jsonの情報を読み取ってテンプレから指定パスでHTMLを生成する。
   - 個別値の入力ができないため、テンプレートからコピー＆リネーム時にHTMLファイルを出力する。
 
## LICENSE

 - Released under the [MIT Licenses](https://tldrlegal.com/license/mit-license). 