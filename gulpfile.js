'use strict';
var gulp = require('gulp');
var fs = require('fs');
var del = require('del');
var ejs = require('gulp-ejs');
var browserify = require('browserify');
var notify = require('gulp-notify');
var cache = require('gulp-cached');
var runSequence = require('run-sequence');
var source = require('vinyl-source-stream');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var compass = require('gulp-compass');
var jsdoc = require('gulp-jsdoc');
var shell = require('gulp-shell');
var csscomb = require('gulp-csscomb');
var uglify = require('gulp-uglify');
var webserver = require('gulp-webserver');
var rename = require('gulp-rename');
var PATH = Object.freeze({
	jsSrc: 'src/common/js/*.js',
	jsMain: 'src/common/js/scripts.js',
	jsDist: 'dist/common/js',
    jsDocDist: './jsdoc',
	imgSrc: 'src/common/images/**',
	imgDist: 'dist/common/images',
	cssSrc: 'src/common/css/**/*.scss',
	cssDist: 'dist/common/css'
});
// サイト設定データの読み込み
//var json = JSON.parse(fs.readFileSync("./siteconfig.json"));
var json = require('./siteconfig.json');
console.log(json);

var handleErrors = function(errorTitle){
	return function() {
		var args = Array.prototype.slice.call(arguments);
		notify.onError({
			title: errorTitle,
			message: '<%= error %>'
		}).apply(this, args);
		this.emit('end');
	};
};

// ejsのコンパイル
gulp.task("ejs", function() {
    gulp.src(
        ["./dist/**/*.ejs",'!' + "./src/**/_*.ejs"]
    )
        .pipe(ejs(json))
        .pipe(gulp.dest("dist/"));
});

// 画像圧縮
var imageminWithPath = function(srcStr, distStr) {
    return gulp.src(srcStr)
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
        }))
        .pipe(gulp.dest(distStr));
};
gulp.task('imagemin', function() {return imageminWithPath(PATHS.sourceImages, PATHS.imageminDistRootDir);});

// Brouserifyのコンパイル
gulp.task('browserify', function(){
	return browserify({
		entries: [PATH.jsMain],
		debug: true
	})
		.bundle()
		.on('error', handleErrors('Browserify build failed'))
		.pipe(source('scripts.js'))
		.pipe(gulp.dest(PATH.jsDist));
});
// SCSS・COMPASSのコンパイル
gulp.task('compass', function() {
	return gulp.src([PATH.cssSrc])
		.pipe(cache('compass'))
		.pipe(compass({
			config_file: './config.rb',
			css: 'dist/common/css/',
			sass: 'src/common/css/'
		}))
		.on('error', handleErrors('Compass build failed'))
		.pipe(gulp.dest(PATH.cssDist));
});

// CSS順序の整列
gulp.task('csscomb', function() {
    return gulp.src(PATH.cssDist)
        .pipe(csscomb())
        .pipe(gulp.dest(PATH.cssDist));
});

// スタイルガイドの作成
gulp.task('kss', shell.task(['kss-node --source src/common/css --destination dist/_kss/ --css /common/css/index.css']));

// JSDocの作成
gulp.task('jsdoc', function() {
    return gulp.src(PATH.jsSrc)
        .pipe(jsdoc(PATH.jsDocDist));
});

// JSの圧縮
gulp.task('uglify', function() {
	return gulp.src('dist/common/js/scripts.js')
		.pipe(uglify())
		.pipe(gulp.dest(PATH.jsDist));
});

// webserverの起動
gulp.task('webserver', function() {
	gulp.src('./dist')
		.pipe(webserver({
            livereload: true,
            open: 'http://localhost:8000/'
		}));
});

// distフォルダの削除
gulp.task('clean', function (callback) {
    return del(['./dist']);
});

// 不要フォルダの削除
gulp.task('unnecessary', function (callback) {
	return del([
		'./dist/**/ejs_include',
		'./dist/**/*.ejs',
		'./dist/**/*.scss',
		'./dist/**/*.sass',
		'./dist/common/js/modules',
		'./dist/common/css/modules'
	]);
});

// ファイルのコピー
gulp.task('copy', function() {
	return gulp.src(
		//[ 'src/**/*.html','src/**/*.ejs', 'src/**/*.js', 'src/**/*.css' ],
		[ 'src/**/*.*' ],
		{ base: 'src' }
	)
		.pipe( gulp.dest( 'dist' ) );
});

// ビルド
gulp.task('build', function(callback) {
	return runSequence(
        'clean',
		'copy',
        ['browserify', 'ejs', 'compass'],
		'csscomb',
        'uglify',
		'unnecessary',
        callback
    );
});

// watchタスク
gulp.task('runsequence', function(callback) {
    return runSequence(
        'clean',
        'copy',
        ['browserify', 'ejs', 'compass'],
        'csscomb',
        callback
    );
});

// ディレクトリ監視
gulp.task('watch', ['webserver'], function() {
	//gulp.watch([PATH.jsSrc], ['browserify']);
	//gulp.watch([PATH.cssSrc], ['compass']);
    gulp.watch(['src/**/*.*'],['runsequence']);
});
