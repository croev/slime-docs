###
# Compass
###

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "dist/common/css/"
sass_dir = "src/common/css/"
images_dir = "dist/common/images/"
javascripts_dir = "dist/common/js/"

output_style = :expanded
preferred_syntax = :scss